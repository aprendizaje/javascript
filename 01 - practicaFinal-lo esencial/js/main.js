var estudiantes = [
    {
        "codigo": "001",
        "nombre": "Ariel Yacelly",
        "nota1": 58,
        "nota2": 60,
        "nota3": 75
    }, {
        "codigo": "002",
        "nombre": "Maylee Perez",
        "nota1": 51,
        "nota2": 51,
        "nota3": 45
    }, {
        "codigo": "003",
        "nombre": "Erick Yacelly",
        "nota1": 15,
        "nota2": 29,
        "nota3": 20
    }, {
        "codigo": "004",
        "nombre": "Brayan Yacelly",
        "nota1": 25,
        "nota2": 50,
        "nota3": 45
    }, {
        "codigo": "005",
        "nombre": "Dora Ramos",
        "nota1": 32,
        "nota2": 34,
        "nota3": 45
    }, {
        "codigo": "006",
        "nombre": "Marlene Escobar",
        "nota1": 40,
        "nota2": 50,
        "nota3": 45
    }, {
        "codigo": "007",
        "nombre": "Wilson Cassia",
        "nota1": 41,
        "nota2": 34,
        "nota3": 45
    }, {
        "codigo": "008",
        "nombre": "Carlos Robbie",
        "nota1": 10,
        "nota2": 14,
        "nota3": 45
    }, {
        "codigo": "009",
        "nombre": "Jorge Mollinedo",
        "nota1": 15,
        "nota2": 46,
        "nota3": 48
    }, {
        "codigo": "010",
        "nombre": "Carmen Oczachoque",
        "nota1": 90,
        "nota2": 94,
        "nota3": 85
    }
];
// Datos de JSON
function datosJSON(json) {
    var salida = "";
    for (var i = 0; i < json.length; i++) {
        salida += '<li class="items">' + '<p>Código: <span id="item1">' + json[i].codigo + '</span></p>' + '<p>Nombre: <span id="item2">' + json[i].nombre + '</span></p>' + '<p>Notas: <span id="item3">' + json[i].nota1 + " - " + json[i].nota2 + " - " + json[i].nota3 + '</span></p></li>';
    }
    document.getElementById("listado").innerHTML = salida;
}
// Calcular la nota de promedios
function calcularPromedios(json) {
    var salidaPromedio = "";
    for (var i = 0; i < json.length; i++) {
        var colorResaltar = "resaltarVerde";
        var calcularN = (json[i].nota1 + json[i].nota2 + json[i].nota3) / 3;
        var xSub = calcularN.toFixed(1);
        if(xSub<51){
                colorResaltar = "resaltarRojo";
        }
        salidaPromedio += '<li class="items">' + '<p>Código: <span id="item1">' + json[i].codigo + '</span></p>' + '<p>Nombre: <span id="item2">' + json[i].nombre + '</span></p>' + '<p>Nota promedio: <span id="item3" class="'+colorResaltar+'">' + xSub + '</span></p></li>';
    }
    document.getElementById("listado").innerHTML = salidaPromedio;
}
// Mayor notas de promedios
function notasMayor(json) {
    var promedioMayor = "";
    var auxiliar = 0;
    for (var i = 0; i < json.length; i++) {
        var calcularN = (json[i].nota1 + json[i].nota2 + json[i].nota3) / 3;
        var xSub = calcularN.toFixed(1);
        if (xSub >= auxiliar) {

            promedioMayor = '<li class="items">' + '<p>Código: <span id="item1">' + json[i].codigo + '</span></p>' + '<p>Nombre: <span id="item2">' + json[i].nombre + '</span></p>' + '<p>Nota promedio mayor: <span id="item3" class="resaltarVerde">' + xSub + '</span></p></li>';
            auxiliar = calcularN;
        }
    }
    document.getElementById("listado").innerHTML = promedioMayor;
}
// Menor notas de promedios
function notasMenor(json) {
    var pos = 0;
    var promedioMenor = "";
    var auxiliar = 100;

    for (var i = 0; i < json.length; i++) {
        var calcularN = (json[i].nota1 + json[i].nota2 + json[i].nota3) / 3;
        var xSub = calcularN.toFixed(1);
        if (xSub <= auxiliar) {
            pos = i;
            promedioMenor = '<li class="items">' + '<p>Código: <span id="item1">' + json[i].codigo + '</span></p>' + '<p>Nombre: <span id="item2">' + json[i].nombre + '</span></p>' + '<p>Nota promedio menor: <span id="item3" class="resaltarRojo">' + xSub + '</span></p></li>';
            auxiliar = xSub;
        }
    }
    document.getElementById("listado").innerHTML = promedioMenor;
}


// Mostrar todos los estudiantes
function mostrarTodos() {
    datosJSON(estudiantes);
}
// Mostrar total nota de promedio
function mostrarPromedio() {
    calcularPromedios(estudiantes);
}
// Mostrar la mayor nota de promedios
function mostrarMayor() {
    notasMayor(estudiantes);
}
// Mostrar el menor nota de promedios
function mostrarMenor() {
    notasMenor(estudiantes);
}