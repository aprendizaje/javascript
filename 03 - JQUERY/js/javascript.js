//Funciones de jquery modulo


//Funciones de la TABLA
function editarRegistro(id) {
    for (var i = 0; i < localStorage.length; i++) {
        var clave = localStorage.key(i);
        if (clave == id) {
            registro = $.parseJSON(localStorage.getItem(clave));

            $("#campo1").val(registro.id);
            $("#campo2").val(registro.nombre);
            $("#campo3").val(registro.nota);
        }
    }
}
//Funcion que elimina los registros de la tabla]
function eliminarRegistro(id) {
    localStorage.removeItem(id);
    //Mostrar Tabla
    listarEstudiantes();
}
//----------

//Funcion para mostrar los estudiantes registrados en una tabla
function listarEstudiantes() {
    var tabla = "";
    var parrafo1 = $("#p1");

    tabla += '<table id="tabla" class="table">';
    tabla += '<thead class="thead-inverse">';
    tabla += '<th>ID</th>';
    tabla += '<th>NOMBRE COMPLETO</th>';
    tabla += '<th>NOTA</th>';
    tabla += '<th>EDITAR</th>';
    tabla += '<th>ELIMINAR</th>';
    tabla += '</thead>';

    for (var i = 0; i < localStorage.length; i++) {
        var clave = localStorage.key(i);
        var registro = $.parseJSON(localStorage.getItem(clave));

        tabla += '<tr>';
        tabla += '<td id="td1">' + registro.id + '</td>';
        tabla += '<td>' + registro.nombre + '</td>';
        tabla += '<td>' + registro.nota + '</td>';
        tabla += '<td><button id="btnR" onclick="editarRegistro(\'' + registro.id + '\');">Editar</button></td>';
        tabla += '<td><button id="btnE" onclick="eliminarRegistro(\'' + registro.id + '\');">Eliminar</button></td>';
        tabla += '</tr>';
    }
    tabla += '</table>';
    $(parrafo1).html(tabla);
}

//funcion que limpia la pantalla
function restablecer(contador) {
    $("#campo1").val(contador);
    $("#campo2").val("");
    $("#campo3").val("");

}
//cargando los datos
$(document).ready(function () {

    $("#campo2,#campo2").focus(function () {
        $(this).css("background-color", "#EEF4B7");
    });

    //Ver tabla de Estudiantes
    listarEstudiantes();
    //Crear un id automatico
    var contador;
    if (localStorage.length > 0) {
        contador = localStorage.length + 1;
    } else {
        contador = 1;
    }
    //campo del codigo
    $("#campo1").val(contador);

    //Funcion para registrar estudiantes
    $(".submit").click(function () {
        var id = $("#campo1").val();
        var nombre = $("#campo2").val();
        var nota = $("#campo3").val();


        if ((nombre, nota).length === 0) {
            return;

        } else {
            var registro = {
                id: id,
                nombre: nombre,
                nota: nota
            }
        }

        localStorage.setItem(id, JSON.stringify(registro));
        contador = localStorage.length + 1;
        listarEstudiantes();
        alert("El alumno " + registro.nombre + " fue registrado!!");
        restablecer(contador);
    });

    //Esta función calcula la nota promedio de los alumnos
    $("#btn2").click(function () {
        //Validar para que cuando se haga click al boton no de ningun tipo de resultado
        if (localStorage.length === 0) {
            return false;
        } else {
            //Correr LocalStorage para que de resultado de nota mas alta
            var suma = 0.0;
            for (var i = 0; i < localStorage.length; i++) {
                var clave = localStorage.key(i);
                var registro = $.parseJSON(localStorage.getItem(clave));
                suma += parseInt(registro.nota);
                var prom = suma / localStorage.length;
            }
            alert("La nota promedio es: " + prom.toFixed(2));
        }
    });
    //Esta funcion calcula las notas más altas obtenidas por los alumnos
    $("#btn3").click(function () {

        //Validar para que cuando se haga click al boton no de ningun tipo de resultado
        if (localStorage.length === 0) {
            return false;
        } else {
            //Correr LocalStorage para que de resultado de nota mas alta
            var Nmax = 0;
            for (var i = 0; i < localStorage.length; i++) {
                var clave = localStorage.key(i);
                var registro = $.parseJSON(localStorage.getItem(clave));
                if (Nmax < registro.nota) {
                    Nmax = parseInt(registro.nota);
                }
            }
            alert("La nota maxima es: " + Nmax);
        }
    });
    //Esta función calcula la nota mas baja de los alumnos
    $("#btn4").click(function () {
        if (localStorage.length === 0) {
            return false;
        } else {
            var Nmin = 100;
            for (var i = 0; i < localStorage.length; i++) {
                var clave = localStorage.key(i);
                var registro = $.parseJSON(localStorage.getItem(clave));
                if (Nmin > registro.nota) {
                    Nmin = parseInt(registro.nota);
                }
            }
            alert("La nota minima es: " + Nmin);
        }
    });
});

